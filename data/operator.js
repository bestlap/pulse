/**
 * @copyright 2024 Best Lap. All rights reserved.
 */

const events = [
  { timestamp: '1628764800', positions: [0, 1, 2] },
  { timestamp: '1628851200', positions: [0, 1, 2] },
  { timestamp: '1628937600', positions: [0, 1, 2] },
  { timestamp: '1629024000', positions: [0, 1, 2] },
  { timestamp: '1629110400', positions: [0, 1, 2] },
  { timestamp: '1629196800', positions: [0, 1, 2] },
  { timestamp: '1629283200', positions: [1, 0, 2] },
  { timestamp: '1629369600', positions: [1, 0, 2] },
  { timestamp: '1629456000', positions: [1, 0, 2] },
  { timestamp: '1629542400', positions: [0] },
];

const cars = ['100', '204', '342'];

function getTimestamps(cars, events) {
  let leader = -1;

  return events.reduce((all, { timestamp, positions }) => {
    leader++;

    if (leader >= positions.length) {
      leader = 0;
    }

    const key = cars[positions[leader]];

    all[key] = all[key] || [];
    all[key].push(timestamp);

    return all;
  }, {});
}

const timestamps = getTimestamps(cars, events);
const heat = { cars, timestamps };

console.log(heat);
