/**
 * @copyright 2024 Best Lap. All rights reserved.
 */

const events = [
    { timestamp: '1628764800', car: '1' },
    { timestamp: '1628851200', car: '2' },
    { timestamp: '1628937600', car: '3' },
    { timestamp: '1629024000', car: '1' },
    { timestamp: '1629110400', car: '2' },
    { timestamp: '1629196800', car: '3' },
    { timestamp: '1629283200', car: '2' },
    { timestamp: '1629369600', car: '1' },
    { timestamp: '1629456000', car: '3' },
    { timestamp: '1629542400', car: '1' },
];

function getTimestamps(cars, events) {
    // Assign timestamps to cars
    return events.reduce((all, { timestamp, car }) => {
        const key = cars[+car - 1];

        all[key] = all[key] || [];
        all[key].push(timestamp);

        return all;
    }, {});
}

const cars = ['100', '204', '342'];
const timestamps = getTimestamps(cars, events);
const round = { cars, timestamps };

// Expected output:
// const round = {
//     "cars": ["100", "204", "342"],
//     "timestamps": {
//         "100": [1628764800, 1629024000, 1629369600, 1629542400],
//         "204": [1628851200, 1629110400, 1629283200],
//         "342": [1628937600, 1629196800, 1629456000]
//     }
// };

console.log(round);
