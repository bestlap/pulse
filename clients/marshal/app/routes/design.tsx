import type { MetaFunction } from '@remix-run/node';

import { PageHeader } from '~/components';
import Prototype from '~/components/pitlane-control/prototype';

export const meta: MetaFunction = () => {
  return [
    { title: 'Design Room' },
    { name: 'description', content: 'Best Lap Design Room' },
  ];
};

export default function Design () {
  return (
    <>
      <PageHeader title="Design Room" />

      <main className="design">
        <Prototype
          numbers={{
            track: [],
            ready: ['103', '204', '305', '406'],
            q: ['107', '208', '309', '110', '211', '312', '413', '514'],
            halt: ['115', '216'],
            out: ['117']
          }}
          session={{
            id: 1,
            heat: 0,
            complete: false,
            final: false
          }}
          ontrack={4}
        />
      </main>
    </>
  );
}
