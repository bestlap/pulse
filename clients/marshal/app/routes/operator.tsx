import type { ActionFunctionArgs, MetaFunction } from '@remix-run/node';
import type { OnUpdateArgs } from '~/components/timestamp-list/types';
import type { OperatorData } from '~/types/operator';

import { useRef, useState, useEffect, useCallback } from 'react';
import { json } from '@remix-run/node';
import { Form, useLoaderData } from '@remix-run/react';

import { PageHeader, TimestampList } from '~/components';
import { useSocketData } from '~/hooks/useSocketData';

import {
  getNumbersData,
  getOperatorData,
  readFormData,
  patchOperatorData,
} from '~/helpers/operator';

export const meta: MetaFunction = () => {
  return [
    { title: 'Operator' },
    { name: 'description', content: 'Best Lap Operator' },
  ];
};

export const loader = async () => {
  const baseUrl = process.env.CHRONO_BASE_URL as string;

  const numbers = await getNumbersData(baseUrl);
  const operator = await getOperatorData(baseUrl);

  return json({ numbers, operator });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const serverUrl = process.env.CHRONO_BASE_URL as string;

  const json = await patchOperatorData(serverUrl, await readFormData(request));

  console.log(json);

  return null;
};

import '~/styles/operator.css';

export default function Operator () {
  const { numbers, operator } = useLoaderData<typeof loader>();
  const timestamps = useSocketData<OperatorData[] | null>('operator', operator);

  const formRef = useRef<HTMLFormElement>(null);

  const [timestamp, setTimestamp] = useState<string>('');
  const [positions, setPositions] = useState<string>('');

  const handleUpdate = useCallback(({ timestamp, positions }: OnUpdateArgs) => {
    setTimestamp(String(timestamp));
    setPositions(positions.join(','));
  }, []);

  useEffect(() => {
    if (timestamp && positions) {
      formRef.current?.submit();
    }
  }, [timestamp, positions]);

  return (
    <>
      <PageHeader title="Operator" />

      <main className="operator">
        <TimestampList
          locale="et"
          cars={numbers?.track}
          items={timestamps}
          onUpdate={handleUpdate}
        ></TimestampList>

        <Form method="post" ref={formRef}>
          <input type="hidden" name="timestamp" value={timestamp} />
          <input type="hidden" name="positions" value={positions} />
        </Form>
      </main>
    </>
  );
}
