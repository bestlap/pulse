import type { MetaFunction } from "@remix-run/node";

import { PageHeader, VerticalNav } from '~/components';

export const meta: MetaFunction = () => {
  return [
    { title: "Mashal" },
    { name: "description", content: "Best Lap Marshal" },
  ];
};

const routes = [
  {
    href: '/stopwatch',
    text: 'Stopwatch',
  },
  {
    href: '/operator',
    text: 'Operator',
  },
  {
    href: '/pitlane',
    text: 'Pitlane',
  },
];

export default function Index () {
  return (
    <>
      <PageHeader title="Marshal" />

      <main>
        <VerticalNav items={routes} />
      </main>
    </>
  );
}
