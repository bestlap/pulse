import type { ActionFunction, MetaFunction } from '@remix-run/node';
import { NumbersData } from '~/types';

import { useCallback, useEffect } from 'react';
import { json } from '@remix-run/node';
import { useLoaderData, useRouteLoaderData, useFetcher } from '@remix-run/react';

import { PageHeader } from '~/components';
import PitlaneControl from '~/components/pitlane-control';
import Rotator from '~/components/pitlane-control/rotator';

import { useSocketData } from '~/hooks/useSocketData';

import { getNumbersData, putPitlaneData } from '~/helpers/pitlane';

import '~/styles/index.scss';
import '~/styles/pitlane.scss';

export const meta: MetaFunction = () => {
  return [
    { title: 'Pitlane Control' },
    { name: 'description', content: 'Best Lap Pitlane Control' },
  ];
};

export const loader = async () => {
  const baseUrl = process.env.CHRONO_BASE_URL as string;

  const pitlane = await getNumbersData(baseUrl);

  return json(pitlane);
};

export const action: ActionFunction = async ({ request }) => {
  const serverUrl = process.env.CHRONO_BASE_URL as string;
  const updatedData = await request.json();

  const json = await putPitlaneData(serverUrl, updatedData);

  console.log("Updated data:", json);

  return null;
};

export default function Pitlane () {
  const loaderData = useLoaderData<typeof loader>();
  const fetcher = useFetcher<typeof loader>();
  const data = fetcher.data || loaderData;
  const socket = useSocketData<NumbersData>('pitlane', data as NumbersData);
  const state = Object.fromEntries(
    Object.entries(socket).filter(([, value]) => Array.isArray(value))
  );

  const root = useRouteLoaderData("root");

  console.log("All data:", state, root);

  const revalidate = useCallback(() => {
    if (document.hidden) {
      return;
    }

    fetcher.load("/pitlane");
  }, [fetcher]);

  const handleUpdate = useCallback((updatedData: NumbersData) => {
    fetcher.submit(updatedData,
      {
        method: "POST",
        encType: "application/json",
      }
    );
  }, [fetcher]);

  useEffect(() => {
    document.addEventListener('visibilitychange', revalidate);
    window.addEventListener('pageshow', revalidate);
    window.addEventListener('focus', revalidate);

    return () => {
      document.removeEventListener("visibilitychange", revalidate);
      window.removeEventListener("pageshow", revalidate);
      window.removeEventListener("focus", revalidate);
    };
  }, [revalidate]);

  return (
    <>
      <PageHeader title="Pitlane" />

      <main className="pitlane">
        <div className="numbers">
          <Rotator state={state} onUpdate={handleUpdate} />
          <PitlaneControl state={state} onUpdate={handleUpdate} />
        </div>
      </main>
    </>
  );
}
