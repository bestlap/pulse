import type { MetaFunction } from '@remix-run/node';

import { Form } from '@remix-run/react';
import { PageHeader } from '~/components';

export const meta: MetaFunction = () => {
  return [
    { title: 'Stopwatch' },
    { name: 'description', content: 'Best Lap Stopwatch' },
  ];
};

export const action = async () => {
  const serverUrl = process.env.CHRONO_BASE_URL as string;

  const response = await fetch(`${serverUrl}/stopwatch`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ source: 'stopwatch' }),
  });

  const json = await response.json();

  console.log(json);

  return null;
};

import '~/styles/stopwatch.css';

export default function Stopwatch () {
  return (
    <>
      <PageHeader title="Stopwatch" />

      <main className="stopwatch">
        <Form method="post">
          <button>Lap</button>
        </Form>
      </main>
    </>
  );
}
