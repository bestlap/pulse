import type { SocketClient } from '~/types';

import { useState, useEffect } from 'react';

import { json } from '@remix-run/node';
import {
  Links,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from '@remix-run/react';

import { connect } from '~/services/ws.client';
import { wsContext } from '~/services/ws.context';

import '~/styles/reset.css';
import '~/styles/variables.css';
import '~/styles/defaults.css';

import { getRaceData, getPilotsData, getCarsData, getClassesData, getTeamsData } from './helpers/pitlane';

export const handle = { id: "root" };

export const loader = async () => {
  const baseUrl = process.env.CHRONO_BASE_URL as string;

  const race = await getRaceData(baseUrl);
  const pilots = await getPilotsData(baseUrl);
  const cars = await getCarsData(baseUrl);
  const classes = await getClassesData(baseUrl);
  const teams = await getTeamsData(baseUrl);

  return json({ race, pilots, cars, classes, teams });
};


export function Layout ({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body>
        {children}
        <ScrollRestoration />
        <Scripts />
      </body>
    </html>
  );
}

export default function App () {
  const [socket, setSocket] = useState<SocketClient>();

  useEffect(() => {
    const connection = connect();

    setSocket(connection);

    return () => {
      connection.close();
    };
  }, []);

  return (
    <wsContext.Provider value={socket}>
      <Outlet />
    </wsContext.Provider>
  );
}
