import { useState, useEffect, useContext } from 'react';
import { wsContext } from '~/services/ws.context';

export function useSocketData<T> (eventName: string, defaultValue: T): T {
  const [socketData, setSocketData] = useState<T>(defaultValue);

  const socket = useContext(wsContext);

  useEffect(() => {
    if (!socket) return;

    socket.on(eventName, (data: T) => {
      setSocketData(data);
    });
  }, [socket, eventName]);

  return socketData;
}
