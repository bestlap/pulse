export type { FormattedTimestampProps } from './types';

export default function FormattedTimestamp (props: FormattedTimestampProps) {
  const { locale, value, date, time, millis } = props;

  const dtf = new Intl.DateTimeFormat(locale, {
    year: date ? 'numeric' : undefined,
    month: date ? 'short' : undefined,
    day: date ? 'numeric' : undefined,
    hour: time ? '2-digit' : undefined,
    minute: time ? '2-digit' : undefined,
    second: time ? '2-digit' : undefined,
    fractionalSecondDigits: millis ? 3 : undefined,
  });

  return dtf.format(new Date(value));
}
