export interface FormattedTimestampProps {
  locale?: string;
  value: number;
  date?: boolean;
  time?: boolean;
  millis?: boolean;
}
