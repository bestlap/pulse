export interface VerticalNavItem {
  href: string;
  text: string;
}

export interface VerticalNavProps {
  items: VerticalNavItem[];
}
