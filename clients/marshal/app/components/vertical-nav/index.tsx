import type { VerticalNavProps } from './types';

import { NavLink } from '@remix-run/react';

import styles from './style.module.css';

export default function VerticalNav (props: VerticalNavProps) {
  return (
    <nav className={styles.vertical}>
      <ul>
        {props.items.map(({ href, text }) => (
          <li key={href}>
            <NavLink to={href}>{text}</NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
}
