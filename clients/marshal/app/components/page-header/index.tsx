import type { PageHeaderProps } from './types';

import styles from './style.module.css';

export default function PageHeader (props: PageHeaderProps) {
  const { title } = props;

  return (
    <header aria-labelledby="h1" className={styles.page}>
      <h1 id="h1">{title}</h1>
    </header>
  );
}
