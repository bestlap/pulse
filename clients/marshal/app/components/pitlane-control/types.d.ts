import { NumbersData } from "~/types";

export interface PitlaneControlProps {
  state: NumbersData;
  onUpdate?: (args: PitlaneData) => void;
}

export interface RotatorProps {
  state: NumbersData;
  onUpdate?: (args: PitlaneData) => void;
}
