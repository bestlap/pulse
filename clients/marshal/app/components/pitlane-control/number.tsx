import type { UniqueIdentifier } from '@dnd-kit/core';
import {
  useSortable,
} from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

interface SortableItemProps {
  containerId?: UniqueIdentifier;
  id: UniqueIdentifier;
  index?: number;
  handle?: boolean;
  disabled?: boolean;
  style? (args: unknown): React.CSSProperties;
  getIndex?( id: UniqueIdentifier): number;
  renderItem? (): React.ReactElement;
  wrapperStyle?({index}: {index: number}): React.CSSProperties;
  children?: React.ReactNode;
}

export default function Number ({
  id,
  children,
}: SortableItemProps) {
  const {
    setNodeRef,
    listeners,
    attributes,
    transform,
    transition,
    isDragging,
  } = useSortable({
    id,
  });


  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
    opacity: isDragging ? 0.5 : 1,
  };

  return (
    <li ref={setNodeRef} style={style} {...listeners} {...attributes}>
      {children}
    </li>
  );
}
