import React, { useState, useCallback, useEffect, useRef, KeyboardEvent as ReactKeyboardEvent } from 'react';
import styles from './dropdown.module.scss';

interface DropdownProps {
  label: string;
  children: React.ReactNode;
}

export default function Dropdown ({ label, children }: DropdownProps): JSX.Element {
  const [isOpen, setIsOpen] = useState(false);
  const [isAbove, setIsAbove] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);
  const contentRef = useRef<HTMLDivElement>(null);

  const toggleDropdown = useCallback(() => {
    setIsOpen(prev => !prev);
  }, []);

  const closeDropdown = useCallback(() => {
    setIsOpen(false);
  }, []);

  const handleKeyDown = useCallback((event: ReactKeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter' || event.key === ' ') {
      event.preventDefault();
      toggleDropdown();
    } else if (event.key === 'Escape') {
      closeDropdown();
    }
  }, [toggleDropdown, closeDropdown]);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
        closeDropdown();
      }
    };

    const handleEscapeKey = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        closeDropdown();
      }
    };

    if (isOpen) {
      document.addEventListener('mousedown', handleClickOutside);
      document.addEventListener('keydown', handleEscapeKey);

      // Check if dropdown should be displayed above
      if (dropdownRef.current && contentRef.current) {
        const dropdownRect = dropdownRef.current.getBoundingClientRect();
        const contentHeight = contentRef.current.offsetHeight;
        const windowHeight = window.innerHeight;
        setIsAbove(dropdownRect.bottom + contentHeight > windowHeight);
      }
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
      document.removeEventListener('keydown', handleEscapeKey);
    };
  }, [isOpen, closeDropdown]);

  return (
    <div className={styles['dropdown']} ref={dropdownRef}>
      <div
        className={styles['dropdown__trigger']}
        onClick={toggleDropdown}
        onKeyDown={handleKeyDown}
        tabIndex={0}
        role="button"
        aria-haspopup="true"
        aria-expanded={isOpen}
      >
        {label}
      </div>
      <div
        ref={contentRef}
        className={`${styles['dropdown__content']} ${isOpen ? styles['-open'] : ''} ${isAbove ? styles['-above'] : ''}`}
      >
        {children}
      </div>
    </div>
  );
}
