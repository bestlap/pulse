
import type { PitlaneControlProps } from './types';
import type { NumbersData } from '~/types';

import { useState, useRef, useCallback, useEffect } from 'react';

import {
  DndContext,
  DragOverlay,
  MouseSensor,
  TouchSensor,
  KeyboardSensor,
  useSensor,
  useSensors,
  UniqueIdentifier,
  CollisionDetection,
  getFirstCollision,
  closestCenter,
  pointerWithin,
  rectIntersection,
  DragStartEvent,
  DragOverEvent,
  DropAnimation,
  MeasuringStrategy,
  defaultDropAnimationSideEffects,
} from '@dnd-kit/core';
import {
  SortableContext,
  arrayMove,
  horizontalListSortingStrategy,
} from '@dnd-kit/sortable';

import { coordinateGetter } from './multipleContainersKeyboardCoordinates';

import Pool from './pool';
import Number from './number';

const dropAnimation: DropAnimation = {
  sideEffects: defaultDropAnimationSideEffects({
    styles: {
      active: {
        opacity: '0.5',
      },
    },
  }),
};

export default function PitlaneControl (props: PitlaneControlProps) {
  const { state, onUpdate } = props;
  const [numbers, setNumbers] = useState<NumbersData>(state);
  const [clonedItems, setClonedItems] = useState<NumbersData | null>(null);
  const [activeId, setActiveId] = useState<UniqueIdentifier | null>(null);
  const lastOverId = useRef<UniqueIdentifier | null>(null);
  const recentlyMovedToNewContainer = useRef(false);
  const containers = Object.keys(numbers) as UniqueIdentifier[]

  // Leaving this here for reference
  // const numbers: KeyArray<NumbersDataKeys>[] = ['track', 'ready', 'q', 'halt', 'out'];
  // const numbers: NumbersDataKeys[] = ['track', 'ready', 'q', 'halt', 'out'];
  // const race: KeyArray<RaceStateKeys>[] = ['session', 'heat'];
  // const race: RaceStateKeys[] = ['session', 'heat'];

  /// Object.entries(items).filter(([, value]) => Array.isArray(value)).map(([key, value]) => console.log(key, value));

  const sensors = useSensors(
    useSensor(MouseSensor),
    useSensor(TouchSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter,
    })
  );

  const handleDragStart = ({active}: DragStartEvent) => {
    setActiveId(active.id);
    setClonedItems(numbers);
  }

  const handleDragOver = ({active, over}: DragOverEvent) => {
    const overId = over?.id;

    if (overId == null || active.id in numbers) {
      return;
    }

    const overContainer = findContainer(overId);
    const activeContainer = findContainer(active.id);

    if (!overContainer || !activeContainer) {
      return;
    }

    if (activeContainer !== overContainer) {
      setNumbers((items) => {
        const activeItems = items[activeContainer];
        const overItems = items[overContainer];
        const overIndex = overItems.indexOf(overId);
        const activeIndex = activeItems.indexOf(active.id);

        let newIndex: number;

        if (overId in items) {
          newIndex = overItems.length + 1;
        } else {
          const isBelowOverItem =
            over &&
            active.rect.current.translated &&
            active.rect.current.translated.top >
              over.rect.top + over.rect.height;

          const modifier = isBelowOverItem ? 1 : 0;

          newIndex =
            overIndex >= 0 ? overIndex + modifier : overItems.length + 1;
        }

        recentlyMovedToNewContainer.current = true;

        return {
          ...items,
          [activeContainer]: items[activeContainer].filter(
            (item) => item !== active.id
          ),
          [overContainer]: [
            ...items[overContainer].slice(0, newIndex),
            items[activeContainer][activeIndex],
            ...items[overContainer].slice(
              newIndex,
              items[overContainer].length
            ),
          ],
        };
      });
    } else {
      const activeIndex = numbers[activeContainer].indexOf(active.id);
      const overIndex = numbers[overContainer].indexOf(overId);

      if (activeIndex !== overIndex) {
        setNumbers((numbers) => ({
          ...numbers,
          [overContainer]: arrayMove(
            numbers[overContainer],
            activeIndex,
            overIndex
          ),
        }));
      }
    }
  };

  const handleDragEnd = () => {
    setActiveId(null);

    onUpdate && onUpdate(numbers);
  };

  const collisionDetectionStrategy: CollisionDetection = useCallback(
    (args) => {
      if (activeId && activeId in numbers) {
        return closestCenter({
          ...args,
          droppableContainers: args.droppableContainers.filter(
            (container) => container.id in numbers
          ),
        });
      }

      // Start by finding any intersecting droppable
      const pointerIntersections = pointerWithin(args);
      const intersections =
        pointerIntersections.length > 0
          ? // If there are droppables intersecting with the pointer, return those
            pointerIntersections
          : rectIntersection(args);
      let overId = getFirstCollision(intersections, 'id');

      if (overId != null) {
        if (overId in numbers) {
          const containerItems = numbers[overId];

          // If a container is matched and it contains items (columns 'A', 'B', 'C')
          if (containerItems.length > 0) {
            // Return the closest droppable within that container
            overId = closestCenter({
              ...args,
              droppableContainers: args.droppableContainers.filter(
                (container) =>
                  container.id !== overId &&
                  containerItems.includes(container.id)
              ),
            })[0]?.id;
          }
        }

        lastOverId.current = overId;

        return [{id: overId}];
      }

      // When a draggable item moves to a new container, the layout may shift
      // and the `overId` may become `null`. We manually set the cached `lastOverId`
      // to the id of the draggable item that was moved to the new container, otherwise
      // the previous `overId` will be returned which can cause items to incorrectly shift positions
      if (recentlyMovedToNewContainer.current) {
        lastOverId.current = activeId;
      }

      // If no droppable is matched, return the last match
      return lastOverId.current ? [{id: lastOverId.current}] : [];
    },
    [activeId, numbers]
  );

  const findContainer = (id: UniqueIdentifier) => {
    if (id in numbers) {
      return id;
    }

    return Object.keys(numbers).find((key) => numbers[key].includes(id));
  };

  const onDragCancel = () => {
    if (clonedItems) {
      // Reset items to their original state in case items have been
      // Dragged across containers
      setNumbers(clonedItems);
    }

    setActiveId(null);
    setClonedItems(null);
  };

  useEffect(() => {
    setNumbers(state);
  }, [state]);

  useEffect(() => {
    requestAnimationFrame(() => {
      recentlyMovedToNewContainer.current = false;
    });
  }, [numbers]);

  return (
    <DndContext
      id="pitlane-dnd-context"
      sensors={sensors}
      collisionDetection={collisionDetectionStrategy}
      measuring={{
        droppable: {
          strategy: MeasuringStrategy.Always,
        },
      }}
      onDragStart={handleDragStart}
      onDragOver={handleDragOver}
      onDragEnd={handleDragEnd}
      onDragCancel={onDragCancel}
    >
      {containers.map((pool) => (
        <Pool key={pool} id={pool}>
          <SortableContext items={numbers[pool]} strategy={horizontalListSortingStrategy}>
              {numbers[pool].map((value) => {
                return (
                  <Number key={value} id={value}>
                    {value}
                  </Number>
                );
              }
            )}
          </SortableContext>
        </Pool>
      ))}
      <DragOverlay adjustScale={true} dropAnimation={dropAnimation}>
        <div className="pitlane-tween">{activeId}</div>
      </DragOverlay>
    </DndContext>
  );
}
