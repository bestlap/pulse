import type { UniqueIdentifier } from '@dnd-kit/core';
import { useDroppable } from '@dnd-kit/core';

interface DroppableProps {
  id: UniqueIdentifier;
  children?: React.ReactNode;
}

export default function Pool (props: DroppableProps) {
  const { id, children } = props;
  const { setNodeRef} = useDroppable({ id });

  return (
    <div className={`reel numbers-${id}`}>
      <ul ref={setNodeRef}>
        {children}
      </ul>
    </div>
  );
}
