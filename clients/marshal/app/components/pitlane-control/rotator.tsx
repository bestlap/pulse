import type { NumbersData } from '~/types';
import type { RotatorProps } from './types';

import { useState } from 'react';
import ConfirmSlider from '../confirm-slider';

const ONTRACK = 4;

const reorder = (numbers: NumbersData, ontrack: number, final: boolean = false): NumbersData => ({
  track: numbers.ready,
  ready: numbers.q.slice(0, ontrack),
  q: final ? numbers.q.slice(ontrack) : [...numbers.q.slice(ontrack), ...numbers.track],
  halt: final ? [...numbers.halt, ...numbers.track] : numbers.halt,
  out: numbers.out,
});

export default function Rotator ({ state, onUpdate }: RotatorProps) {
  const [ontrack, setOntrack] = useState(ONTRACK);
  const [isFinal, setIsFinal] = useState(false);
  const [isConfirmed, setIsConfirmed] = useState<boolean>(false);

  const onConfirm = () => {
    onUpdate && onUpdate(reorder(state, ontrack, isFinal));
    setIsConfirmed(true);
    setTimeout(() => setIsConfirmed(false), 500);
  };

  return (
    <div>
      <select name="ontrack" value={ontrack} onChange={(e) => setOntrack(Number(e.target.value))}>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
      <ConfirmSlider
        onChange={onConfirm}
        value={isConfirmed}
        labels={{
          confirm: "Slide to queue",
          confirmed: "Queued!",
          ariaLabel: "Queue slider",
          keyboardInstruction: "Use arrow keys to move and press Enter to queue"
        }}
      >
          Q
      </ConfirmSlider>
      <button onClick={() => onUpdate ? onUpdate(reorder(state, ontrack, isFinal)) : null}>Q</button>
      <label><input type="checkbox" name="final" onChange={(e) => setIsFinal(e.target.checked)} /> Final session</label>
    </div>
  );
}
