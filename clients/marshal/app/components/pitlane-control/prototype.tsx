import { useState, useCallback } from 'react';
import type { NumbersData, Session, PitlaneData } from '~/types';

import styles from './prototype.module.scss';
import Dropdown from './dropdown';
import ConfirmSlider from '../confirm-slider';

const UNDO_LIMIT = 3; // Keep only the last 3 states
const SESSION_LIMIT = 2; // Assuming 3 sessions total

const reorder = (numbers: NumbersData, ontrack: number, final: boolean = false): NumbersData => ({
  track: numbers.ready,
  ready: numbers.q.slice(0, ontrack),
  q: final ? numbers.q.slice(ontrack) : [...numbers.q.slice(ontrack), ...numbers.track],
  halt: final ? [...numbers.halt, ...numbers.track] : numbers.halt,
  out: numbers.out,
});

export default function Prototype ({
  numbers: initialPool,
  session: initialSession,
  ontrack: initialOntrack = 4,
}: PitlaneData): JSX.Element {
  const [numbers, setNumbers] = useState<NumbersData>(initialPool);
  const [session, setSession] = useState<Session>(initialSession);
  const [ontrack, setOntrack] = useState<number>(initialOntrack);
  const [history, setHistory] = useState<PitlaneData[]>([]);
  const [isConfirmed, setIsConfirmed] = useState<boolean>(false);

  const save = useCallback(() => {
    setHistory(history =>  [{ numbers: { ...numbers }, session: { ...session }, ontrack: ontrack }, ...history].slice(0, UNDO_LIMIT));
  }, [numbers, session, ontrack]);

  const queue = useCallback(() => {
    if (session.final === true && session.complete === true) {
      return;
    }

    save();

    setIsConfirmed(true);
    setTimeout(() => setIsConfirmed(false), 500);

    setNumbers(numbers => reorder(numbers, ontrack, session.final));

    setSession(session => ({
      ...session,
      heat: session.heat + 1
    }));
  }, [ontrack, save, session.final, session.complete]);

  const completeSession = useCallback(() => {
    if (!window.confirm('Are you sure you want to complete this session?')) {
      return;
    }

    setSession(previous => {
      const next = {
        id: previous.id + (previous.final ? 0 : 1),
        heat: 0,
        complete: previous.final ? true : false,
        final: previous.id >= SESSION_LIMIT,
      };

      setNumbers(numbers => ({
        ...numbers,
        track: [],
        q: [...numbers.q, ...numbers.track],
      }));

      setHistory([]); // Reset undo history

      return next;
    });
  }, []);

  const undo = useCallback(() => {
    if (history.length <= 0) {
      return;
    }

    const [lastState, ...newHistory] = history;

    setNumbers(lastState.numbers);
    setSession(lastState.session);
    setHistory(newHistory);
  }, [history]);

  return (
    <div className={styles['pitlane-interface']}>
      <div className={styles['pitlane-interface__main-controls']}>
        <div className={styles['pitlane-interface__on-track']}>
          <select
            id="on-track"
            value={ontrack}
            onChange={(e) => setOntrack(Number(e.target.value))}
          >
            {[1, 2, 3, 4, 5].map(n => (
              <option key={n} value={n}>{n}</option>
            ))}
          </select>
          <label htmlFor="on-track">on track</label>
        </div>
        {/*
        <button
          onClick={handleQ}
          className={styles['pitlane-interface__q-button']}
          disabled={session.isComplete}
        >
          Q ▶
        </button>
        */}
        <ConfirmSlider
          value={isConfirmed}
          onChange={queue}
          labels={{
            confirm: "Slide to queue",
            confirmed: "Queued!",
            ariaLabel: "Queue slider",
            keyboardInstruction: "Use arrow keys to move and press Enter to queue"
          }}
        >
          Q
        </ConfirmSlider>
        <div className={styles['pitlane-interface__heat-info']}>
          <Dropdown label={`Heat: ${session.heat}`}>
            <button onClick={undo} disabled={history.length === 0}>
              Undo
            </button>
          </Dropdown>
        </div>
      </div>

      <div className={styles['pitlane-interface__pools']}>
        {(Object.keys(numbers) as Array<keyof NumbersData>).map(pool => (
          <div key={pool} className={`${styles['pitlane-interface__pool']} ${styles[`pitlane-interface__pool--${pool}`]}`}>
            <h2 className={styles['pitlane-interface__pool-title']}>{pool}</h2>
            <div className={styles['pitlane-interface__numbers']}>
              {numbers[pool].map(number => (
                <div key={number} className={styles['pitlane-interface__number']}>{number}</div>
              ))}
            </div>
          </div>
        ))}
      </div>

      <div className={styles['pitlane-interface__session-controls']}>
        <Dropdown label={`Session: ${session.id}/3`}>
          <button
            onClick={completeSession}
            disabled={session.heat === 0 || session.complete}
          >
            Complete Session
          </button>
        </Dropdown>
        {session.final && (
          <span className={styles['pitlane-interface__final-session']}>⚠ Final Session</span>
        )}
      </div>
    </div>
  );
}
