import React, { useState, useRef, useEffect, useCallback } from 'react';
import style from './style.module.scss';

interface ConfirmSliderProps {
  value: boolean;
  onChange: (newValue: boolean) => void;
  labels: {
    confirm: string;
    confirmed: string;
    ariaLabel: string;
    keyboardInstruction: string;
  };
  children?: React.ReactNode;
}

export default function ConfirmSlider ({
  value,
  onChange,
  labels,
  children
}: ConfirmSliderProps): JSX.Element {
  const [visualPosition, setVisualPosition] = useState(0);
  const [isKeyboardNavigation, setIsKeyboardNavigation] = useState(false);
  const [hasAttemptedConfirm, setHasAttemptedConfirm] = useState(false);
  const [isAnimating, setIsAnimating] = useState(false);

  const containerRef = useRef<HTMLDivElement>(null);
  const sliderRef = useRef<HTMLDivElement>(null);
  const isDraggingRef = useRef(false);
  const sliderWidth = useRef(0);

  const updateVisualPosition = useCallback((newPosition: number, animate: boolean = false) => {
    setVisualPosition(newPosition);
    if (sliderRef.current) {
      sliderRef.current.style.transition = animate ? 'transform .5s ease-out' : '';
      sliderRef.current.style.transform = `translateX(${newPosition}px)`;
    }
  }, []);

  const handleConfirm = useCallback(() => {
    setHasAttemptedConfirm(true);
    onChange(true);
    setIsAnimating(true);
    setTimeout(() => setIsAnimating(false), 300);
  }, [onChange]);

  const resetSlider = useCallback(() => {
    setIsAnimating(true);
    updateVisualPosition(0, true);
    isDraggingRef.current = false;
    containerRef.current?.classList.remove(style['-dragging']);
    setHasAttemptedConfirm(false);
    setTimeout(() => setIsAnimating(false), 300);
  }, [updateVisualPosition]);

  const checkConfirmation = useCallback(() => {
    if (!containerRef.current) return;
    const containerWidth = containerRef.current.offsetWidth;
    const confirmThreshold = containerWidth - sliderWidth.current - 10;

    if (visualPosition >= confirmThreshold) {
      handleConfirm();
    } else {
      resetSlider();
    }
  }, [visualPosition, handleConfirm, resetSlider]);

  const handlePointerDown = (event: React.PointerEvent) => {
    if (value || isAnimating) return;
    isDraggingRef.current = true;
    containerRef.current?.classList.add(style['-dragging']);
    (event.target as HTMLElement).setPointerCapture(event.pointerId);
    setIsKeyboardNavigation(false);
  };

  const handlePointerMove = (event: React.PointerEvent) => {
    if (!isDraggingRef.current || value || isAnimating) return;
    const newPosition = calculateNewPosition(event.clientX);
    updateVisualPosition(newPosition);
    event.preventDefault();
  };

  const handlePointerUp = (event: React.PointerEvent) => {
    if (value || isAnimating) return;
    isDraggingRef.current = false;
    containerRef.current?.classList.remove(style['-dragging']);
    (event.target as HTMLElement).releasePointerCapture(event.pointerId);
    checkConfirmation();
  };

  const calculateNewPosition = (clientX: number): number => {
    if (!containerRef.current) return 0;
    const containerRect = containerRef.current.getBoundingClientRect();
    const maxPosition = containerRect.width - sliderWidth.current;
    return Math.max(0, Math.min(clientX - containerRect.left, maxPosition));
  };

  const handleKeyDown = (event: React.KeyboardEvent) => {
    if (value || isAnimating) return;
    setIsKeyboardNavigation(true);

    const containerWidth = containerRef.current?.offsetWidth || 0;
    const step = containerWidth * 0.5;
    let newPosition = visualPosition;

    switch (event.key) {
      case 'ArrowRight':
        newPosition = Math.min(visualPosition + step, containerWidth - sliderWidth.current);
        break;
      case 'ArrowLeft':
        newPosition = Math.max(visualPosition - step, 0);
        break;
      case 'Home':
        newPosition = 0;
        break;
      case 'End':
        newPosition = containerWidth - sliderWidth.current;
        break;
      case 'Enter':
      case ' ':
        if (visualPosition >= containerWidth - sliderWidth.current - 10) {
          handleConfirm();
        }
        return;
      case 'Escape':
        resetSlider();
        return;
      default:
        return;
    }

    updateVisualPosition(newPosition, true);
    event.preventDefault();
  };

  const handleBlur = () => {
    if (value || isAnimating || isDraggingRef.current) return;
    resetSlider();
  };

  useEffect(() => {
    if (sliderRef.current) {
      sliderWidth.current = sliderRef.current.offsetWidth;
    }
  }, []);

  useEffect(() => {
    if (value) {
      const containerWidth = containerRef.current?.offsetWidth || 0;
      updateVisualPosition(containerWidth - sliderWidth.current, true);
    } else if (hasAttemptedConfirm) {
      resetSlider();
    }
  }, [value, updateVisualPosition, resetSlider, hasAttemptedConfirm]);

  return (
    <div
      ref={containerRef}
      className={`${style.slider} ${value ? style['-confirmed'] : ''} ${isAnimating ? style['-animating'] : ''}`}
      aria-label={labels.ariaLabel}
    >
      <div
        className={style.slider__text}
        aria-live="polite"
        style={{ opacity: `${1 - (visualPosition + sliderWidth.current) / (containerRef.current?.offsetWidth || 1)}` }}
      >
        <span>
          {value
            ? labels.confirmed
            : isKeyboardNavigation
              ? labels.keyboardInstruction
              : labels.confirm}
        </span>
      </div>
      <div
        className={style.slider__progress}
        style={{ width: `${((visualPosition + sliderWidth.current) / (containerRef.current?.offsetWidth || 1)) * 100}%` }}
        aria-hidden="true"
      />
      <div
        ref={sliderRef}
        className={style.slider__handle}
        onPointerDown={handlePointerDown}
        onPointerMove={handlePointerMove}
        onPointerUp={handlePointerUp}
        onKeyDown={handleKeyDown}
        onBlur={handleBlur}
        style={{ touchAction: 'none' }}
        tabIndex={value || isAnimating ? -1 : 0}
        role="slider"
        aria-disabled={value || isAnimating}
        aria-valuemin={0}
        aria-valuemax={100}
        aria-valuenow={Math.round((visualPosition / (containerRef.current?.offsetWidth || 1)) * 100)}
        aria-valuetext={value ? labels.confirmed : labels.confirm}
      >
        {children}
      </div>
      <div className={style.slider__checkmark}>✓</div>
    </div>
  );
}
