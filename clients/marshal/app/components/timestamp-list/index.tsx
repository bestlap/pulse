import type { TimestampListProps, OnUpdateArgs, OnDeleteArgs } from './types';
import type { OperatorData } from '~/types/operator';

import { useCallback } from 'react';
import { TimestampInfo } from '~/components';

import styles from './style.module.css';

export default function TimestampList (props: TimestampListProps) {
  const { locale, items, cars = [], onUpdate, onDelete } = props;

  const makeKey = ({ timestamp, positions }: OperatorData) => {
    return `${timestamp}_${positions.join(',')}`;
  };

  const handleUpdate = useCallback(
    (args: OnUpdateArgs) => {
      onUpdate?.(args);
    },
    [onUpdate]
  );

  const handleDelete = useCallback(
    (args: OnDeleteArgs) => {
      onDelete?.(args);
    },
    [onDelete]
  );

  return (
    <article className={styles.tsl}>
      <ol>
        {items?.map((entry) => (
          <li key={makeKey(entry)}>
            <TimestampInfo
              locale={locale}
              cars={cars}
              timestamp={entry.timestamp}
              positions={entry.positions}
              onUpdate={handleUpdate}
              onDelete={handleDelete}
            />
          </li>
        ))}
      </ol>
    </article>
  );
}
