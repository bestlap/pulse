import type { OnUpdateArgs, OnDeleteArgs } from '~/components/timestamp-info/types';
import type { OperatorData } from '~/types';

export { OnUpdateArgs, OnDeleteArgs };

export interface TimestampListProps {
  locale?: string;
  cars?: string[];
  items?: OperatorData[] | null,
  onUpdate?: (args: OnUpdateArgs) => void;
  onDelete?: (args: OnDeleteArgs) => void;
}
