import type { TimestampProps, ItemType } from './types';

import { useState, useEffect } from 'react';
import { FormattedTimestamp } from '~/components';
import sortablejs from 'react-sortablejs';

const { ReactSortable } = sortablejs;

import styles from './style.module.css';

export default function Timestamp (props: TimestampProps) {
  const { locale, timestamp, positions, cars = [], onUpdate, onDelete } = props;

  const [items, setItems] = useState<ItemType[]>(
    positions.map((id) => ({ id, name: cars?.[id] }))
  );

  const [reordered, setReordered] = useState<boolean>(false);

  const handleDeleteClick = () => {
    onDelete?.({ timestamp });
  };

  const handleEnd = () => {
    setReordered(true);
  };

  useEffect(() => {
    if (reordered) {
      onUpdate?.({ timestamp, positions: items.map(({ id }) => id) });
      setReordered(false);
    }
  }, [timestamp, items, reordered, onUpdate]);

  return (
    <section aria-labelledby={`ts-${timestamp}`} className={styles.ts}>
      <h2 id={`ts-${timestamp}`}>
        <FormattedTimestamp locale={locale} value={timestamp} time millis />
      </h2>

      <ReactSortable tag="ol" list={items} setList={setItems} onEnd={handleEnd}>
        {items.map((item) => (
          <li key={item.id}>{item.name}</li>
        ))}
      </ReactSortable>

      <button title="Delete" aria-label="Delete" onClick={handleDeleteClick}>
        &#9003;
      </button>
    </section>
  );
}
