import type { ItemInterface } from 'react-sortablejs';

export interface ItemType extends ItemInterface {
  id: number;
  name: string;
}

export interface OnUpdateArgs {
  timestamp: number;
  positions: number[];
}

export interface OnDeleteArgs {
  timestamp: number;
}

export interface TimestampProps {
  locale?: string;
  timestamp: number;
  positions: number[];
  cars?: string[];
  onUpdate?: (args: OnUpdateArgs) => void;
  onDelete?: (args: OnDeleteArgs) => void;
}
