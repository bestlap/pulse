import type { NumbersData, OperatorData } from "~/types";

export const getNumbersData = async (serverUrl : string): Promise<NumbersData | null> => {
  const numbersResponse = await fetch(`${serverUrl}/numbers`);

  if (!numbersResponse.ok) {
    return null;
  }

  return numbersResponse.json() as Promise<NumbersData>;
};

export const getOperatorData = async (serverUrl : string): Promise<OperatorData[] | null> => {
  const operatorResponse = await fetch(`${serverUrl}/operator`);

  if (!operatorResponse.ok) {
    return null;
  }

  return operatorResponse.json() as Promise<OperatorData[]>;
};

export const readFormData = async (request: Request): Promise<OperatorData> => {
  const formData = await request.formData();

  const timestamp = String(formData.get("timestamp"));
  const positions = String(formData.get("positions"));

  return {
    timestamp: +timestamp,
    positions: positions.split(",").map((p) => +p),
  }
};

export const patchOperatorData = async (serverUrl: string, payload: OperatorData) => {
  const response = await fetch(`${serverUrl}/operator`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      source: "operator",
      payload,
    }),
  });

  return response.json();
};

