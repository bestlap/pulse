import type { NumbersData, Race, Pilots, Car, Classes, Teams } from "~/types";

export const getNumbersData = async (serverUrl : string): Promise<NumbersData | null> => {
  const numbersResponse = await fetch(`${serverUrl}/numbers`);

  if (!numbersResponse.ok) {
    return null;
  }

  return numbersResponse.json() as Promise<NumbersData>;
};

export const getRaceData = async (serverUrl : string): Promise<Race | null> => {
  const race = await fetch(`${serverUrl}/race`);

  if (!race.ok) {
    return null;
  }

  return race.json() as Promise<Race>;
};

export const getCarsData = async (serverUrl : string): Promise<Car[] | null> => {
  const cars = await fetch(`${serverUrl}/cars`);

  if (!cars.ok) {
    return null;
  }

  return cars.json() as Promise<Car[]>;
};

export const getPilotsData = async (serverUrl : string): Promise<Pilots | null> => {
  const pilots = await fetch(`${serverUrl}/pilots`);

  if (!pilots.ok) {
    return null;
  }

  return pilots.json() as Promise<Pilots>;
};

export const getClassesData = async (serverUrl : string): Promise<Classes | null> => {
  const classes = await fetch(`${serverUrl}/classes`);

  if (!classes.ok) {
    return null;
  }

  return classes.json() as Promise<Classes>;
};

export const getTeamsData = async (serverUrl : string): Promise<Teams | null> => {
  const teams = await fetch(`${serverUrl}/teams`);

  if (!teams.ok) {
    return null;
  }

  return teams.json() as Promise<Teams>;
};


export const putPitlaneData = async (serverUrl: string, payload: NumbersData) => {
  const response = await fetch(`${serverUrl}/pitlane`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      source: "pitlane",
      payload,
    }),
  });

  return response.json();
};
