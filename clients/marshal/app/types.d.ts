import type { DefaultEventsMap } from '@socket.io/component-emitter';
import type { Socket } from 'socket.io-client';
import type { UniqueIdentifier } from '@dnd-kit/core';

export interface SocketClient extends Socket<DefaultEventsMap, DefaultEventsMap> {}

export interface Race {
  race: string;
  place: string;
  track: string;
  date: string;
  rules: {
    rounds: number;
    laps: number;
    wins: string;
  };
  classes: string[];
  points: number[];
}

export interface Pilots {
  [id: string]: string;
  length: number;
}

export interface Teams {
  [name: string]: string[];
}

export interface Classes {
  [name: string]: number[];
}

export interface Car {
  number: number[];
  mark: string;
  model: string;
  type: string;
  engine: {
    volume: number;
    power: number;
    turbo: boolean;
  };
  wheels: {
    tyres: string;
  };
  year: number;
  color: string;
}

export type NumbersData = Record<UniqueIdentifier, UniqueIdentifier[]>;

/*
export interface NumbersData {
  track: string[],
  ready: string[],
  q: string[],
  halt: string[],
  out: string[]
}
*/

export type NumbersDataKeys = keyof NumbersData;

export interface OperatorData {
  timestamp: number;
  positions: number[];
}

export interface RaceState {
  session: number;
  heat: number;
}

export type RaceStateKeys = keyof RaceState;

export interface PitlaneData extends NumbersData, RaceState {}

export type PitlaneDataKeys = keyof PitlaneData;

export type KeyArray<T> = {
  [K in keyof T]: K;
}[keyof T];
