# Pulse

The system for managing rounds in a race or similar event. The service includes various components and features, such as round management, live stats tracking (including lap times and best lap), a forwarder to distribute data to visitors, endpoints for Radar, Stopwatch, and Operator to process input data, and the distribution of a JSON file called "round.json" to visitors.

## Shared state

-   Cars on track
-   Car positions
-   Timestamps from Radar/Stopwatch (potentially transponders)

## Key players

-   `Core` statful service that have all the data, the source of truth
-   `Echo` message dispatching service to distribute state to visitors
-   `Radar` is tool that takes data from an external service (`Laser`) that sends intersection events from laser measurment tool

    -   `Laser` is a an external service that registeres and sends timestamps (start/finish line crossings)

-   `Marshal` is a web tool that cosists of `Operator` and `Stopwatch`

    -   `Operator` is a web tool that allows to modify order of cars and mark timestamps as accidental (reordering cars and editing race round events)
    -   `Stopwatch` is a web tool to make timestamps
    -   `Pitlane` is a web tool that magages everything within a `Round`: sorts pilots in a queue, starts `Sessions` and `Heats` witin it.

-   `Transponder` 🤞🤞

## Technologies

-   `Core` php that handles all state
    -   `Radar` RESTful receiver (connector)
    -   `Stopwatch` receiving service
    -   `Operator` receiving service
-   `Echo` node.js
-   `Marshal` SPA/Remix
    -   `Operator` web UI js (stateless), receives from/sends to `Core` to php to save state
    -   `Stopwatch` web UI js (stateless), receives from/sends to `Core` to php to save state
    -   `Pitlane` web UI js (stateless), receives from/sends to `Core` to php to save state

## Next steps

-   API/interface design
-   Session management
-   Driver/Follower web UI
-   Live stats: what happening on track; lap times, best lap etc...
-   Forwarder to echo to distribute data to visitors
-   Endpoints for Radar, Stopwatch and Operator that process input data to round.json
-   Distribute round.json to visitors
-   Stopwatch web UI
-   Operator web UI

## Terminology

-   `Season` — collection of race events in one calendar year
-   `Round` — a single race event within a `Season`
-   `Session` — an attempt to set the fastest time in a round. `Round` typically consists of three `Sessions` (or stages).
-   `Heat` is race/attempt within a `Session` where a limited group of drivers attempt to set the fastest lap
