# Stopwatch

An UI interface that creates and sends timestamps by pressing a button.

Server accumulates timestamps received within a `Heat` in a `/heat/stopwatch.json` file.

```json
[
    1628764800, 1628851200, 1628937600, 1629024000, 1629110400, 1629196800,
    1629283200, 1629369600, 1629456000, 1629542400
]
```
