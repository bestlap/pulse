# Operator

Is an UI interface for a race marshal to update driver positions according to changes in drivers order on a track.

Unavailable when the `Laser` service is present.

## UI Data sources

### `/numbers.json`

```json
{
    "track": ["111", "222"],
    "ready": ["250", "180"],
    "q": ["100", "150", "200", "202", "303", "333", "400"],
    "halt": [],
    "out": []
}
```

### `/heats/current.json`

```json
[
    1628764800, 1628851200, 1628937600, 1629024000, 1629110400, 1629196800,
    1629283200, 1629369600, 1629456000, 1629542400
]
```
