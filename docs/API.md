# Services

## Core

### Radar

Receives messages from the `Laser` and saves them in the `Heat` state.

#### Endpoint

POST `/api/radar`

#### Payload

```json
[
    { "timestamp": "1628764800", "car": "1" },
    { "timestamp": "1628851200", "car": "2" },
    { "timestamp": "1628937600", "car": "3" },
    { "timestamp": "1629024000", "car": "1" },
    { "timestamp": "1629110400", "car": "2" },
    { "timestamp": "1629196800", "car": "3" },
    { "timestamp": "1629283200", "car": "2" },
    { "timestamp": "1629369600", "car": "1" },
    { "timestamp": "1629456000", "car": "3" },
    { "timestamp": "1629542400", "car": "1" }
]
```

#### Response

```
200 OK
```

### Stopwatch

#### Endpoint

POST `/api/stopwatch`
Content-Type: text/plain

```
1628764800
```

### Operator

#### Endpoint

POST `/api/operator`
Content-Type: application/json

##### Payload

```json
[
    { "timestamp": "1628764800", "positions": [0, 1, 2] },
    { "timestamp": "1628851200", "positions": [0, 1, 2] },
    { "timestamp": "1628937600", "positions": [0, 1, 2] },
    { "timestamp": "1629024000", "positions": [0, 1, 2] },
    { "timestamp": "1629110400", "positions": [0, 1, 2] },
    { "timestamp": "1629196800", "positions": [0, 1, 2] },
    { "timestamp": "1629283200", "positions": [1, 0, 2] },
    { "timestamp": "1629369600", "positions": [1, 0, 2] },
    { "timestamp": "1629456000", "positions": [1, 0, 2] },
    { "timestamp": "1629542400", "positions": [0] }
]
```

## Echo

### Endpoint

POST `//echo/heat`
Content-Type application/json

#### Payload

```json
{
    "cars": ["100", "204", "342"],
    "positions": [0, 1, 2],
    "timestamps": {
        "100": [1628764800, 1629024000, 1629369600, 1629542400],
        "204": [1628851200, 1629110400, 1629283200],
        "342": [1628937600, 1629196800, 1629456000]
    }
}
```
