#!/bin/bash

IMAGE_TAGNAME=chrono:latest
CONTAINER_NAME=chrono

docker rm $(docker stop $CONTAINER_NAME)

docker rmi $IMAGE_TAGNAME

docker build --file ./dockerfile --tag $IMAGE_TAGNAME .

mkdir -m 777 -p ./.log

docker run \
    --detach \
    --publish 9000:80 \
    --mount type=bind,src=./src,dst=/var/www \
    --mount type=bind,src=./.log,dst=/var/log/apache2 \
    --name $CONTAINER_NAME \
    $IMAGE_TAGNAME
