<?

$request_data = read_request_data('radar.json');

function get_starting_positions($raceData) {
    $carsOnTrack = count($raceData['track'] ?? []);

    if ($carsOnTrack < 1) {
        // 403 Forbidden
        send_json_response(403, [
            'success' => false,
            'message' => 'No cars on the track.',
        ]);

        exit;
    }

    return array_keys(array_fill(0, $carsOnTrack, null));
}

function transform_radar_events($radarEvents, $startingPositions) {
    $timestamps = [];

    $positions = $startingPositions;
    $intersections = 0;

    foreach ($radarEvents as $event) {
        $timestamp = intval($event->{'timestamp'});
        $carNumber = intval($event->{'car'}); // 1-based

        $carsOnTrack = count($positions);
        $atPosition = $intersections % $carsOnTrack;

        // Both are 0-based indexes
        $expectedCar = $positions[$atPosition];
        $actualCar = $carNumber - 1;

        // Unexpected car in position?
        if ($actualCar != $expectedCar) {
            // Find last known position of the actual car
            $actualAt = array_search($actualCar, $positions);

            // Swap actual and expected (overtake)
            $positions[$atPosition] = $actualCar;
            $positions[$actualAt] = $expectedCar;
        }

        $record = [
            'timestamp' => $timestamp,
            'positions' => $positions,
        ];

        array_push($timestamps, $record);

        $intersections++;
    }

    return $timestamps;
}

try {
    $numbersDataFile = __CONFIG__['numbersDataFile'];
    $heatDataFile = __CONFIG__['heatDataFile'];

    $raceData = read_data_file($numbersDataFile) ?? [];
    $startingPositions = get_starting_positions($raceData);
    $heatData = transform_radar_events($request_data, $startingPositions);

    save_data_file($heatDataFile, $heatData);

    send_json_request(getenv('URL_ECHO') . '/operator', $heatData);

    // 201 Created
    send_json_response(201, [
        'success' => true,
        'message' => 'Timestamps saved',
        'payload' => $heatData
    ]);

} catch (Exception $e) {
    // 500 Internal Server Error
    send_json_response(500, [
        'success' => true,
        'message' => $e->getMessage()
    ]);
}
