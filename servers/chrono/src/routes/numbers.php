<?

$request_data = read_request_data('numbers.json');

try {
    $numbers = $request_data->{'payload'};

    $numbersFile = __CONFIG__['numbersDataFile'];

    save_data_file($numbersFile, $numbers);

    send_json_request(getenv('URL_ECHO') . '/numbers', $numbers);

    // 200 OK
    send_json_response(200, [
        'success' => true,
        'message' => 'Numbers reordered',
        'payload' => $numbers
    ]);
} catch (Exception $e) {
    // 500 Internal Server Error
    send_json_response(500, [
        'success' => false,
        'message' => $e->getMessage()
    ]);
}
