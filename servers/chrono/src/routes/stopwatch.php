<?

$request_data = read_request_data('stopwatch.json');

function get_starting_positions($raceData) {
    $carsOnTrack = count($raceData['track'] ?? []);

    if ($carsOnTrack < 1) {
        // 403 Forbidden
        send_json_response(403, [
            'success' => false,
            'message' => 'No cars on the track.',
        ]);

        exit;
    }

    return array_keys(array_fill(0, $carsOnTrack, null));
}

function create_record($heatData, $timestamp, $positions) {
    $intersections = count($heatData);

    if ($intersections > 0) {
        $positions = $heatData[$intersections - 1]['positions'];
    }

    return [
        'timestamp' => $timestamp,
        'positions' => $positions,
    ];
}

try {
    $numbersDataFile = __CONFIG__['numbersDataFile'];
    $heatDataFile = __CONFIG__['heatDataFile'];

    $raceData = read_data_file($numbersDataFile) ?? [];
    $heatData = read_data_file($heatDataFile) ?? [];

    // Consider sending timestamp with request fo better precision.
    // Due to network and filesystem delays the registered timestamp
    // might be milliseconds or even seconds behind the operator's.

    $timestamp = floor(microtime(true) * 1000);
    $positions = get_starting_positions($raceData);

    $record = create_record($heatData, $timestamp, $positions);

    array_push($heatData, $record);
    save_data_file($heatDataFile, $heatData);

    send_json_request(getenv('URL_ECHO') . '/operator', $heatData);

    // 201 Created
    send_json_response(201, [
        'success' => true,
        'message' => 'Timestamp recorded',
        'payload' => $record
    ]);
} catch (Exception $e) {
    // 500 Internal Server Error
    send_json_response(500, [
        'success' => true,
        'message' => $e->getMessage()
    ]);
}
