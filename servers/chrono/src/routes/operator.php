<?

$request_data = read_request_data('operator.json');

function update_positions(&$heatData, $timestamp, $positions) {
    $patching = false;

    foreach ($heatData as &$record) {
        if ($record['timestamp'] == $timestamp) {
            $patching = true;
        }

        if ($patching) {
            $record['positions'] = $positions;
        }
    }

    unset($record);

    return $patching;
}

try {
    $timestamp = $request_data->{'payload'}->{'timestamp'};
    $positions = $request_data->{'payload'}->{'positions'};

    $heatFile = __CONFIG__['heatDataFile'];
    $heatData = read_data_file($heatFile) ?? [];

    $patched = update_positions($heatData, $timestamp, $positions);

    if ($patched) {
        save_data_file($heatFile, $heatData);

        send_json_request(getenv('URL_ECHO') . '/operator', $heatData);
    }

    // 200 OK
    send_json_response(200, [
        'success' => $patched,
        'message' => $patched ? 'Positions updated' : 'Positions unchanged',
        'payload' => $heatData
    ]);

} catch (Exception $e) {
    // 500 Internal Server Error
    send_json_response(500, [
        'success' => true,
        'message' => $e->getMessage()
    ]);
}

