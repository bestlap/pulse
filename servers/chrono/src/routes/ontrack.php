<?

$request_data = read_request_data('ontrack.json');

try {
    $ontrack = $request_data->{'payload'};

    $pitlaneDataFile = __CONFIG__['pitlaneDataFile'];
    $numbersFile = __CONFIG__['numbersDataFile'];
    $sessionDataFile = __CONFIG__['sessionDataFile'];

    $numbersData = read_data_file($numbersFile);
    $sessionData = read_data_file($sessionDataFile);

    $pitlane = [
        'numbers' => $numbersData,
        'session' => $sessionData,
        'ontrack' => $ontrack->ontrack,
    ];

    save_data_file($pitlaneDataFile, $pitlane);

    send_json_request(getenv('URL_ECHO') . '/ontrack', $ontrack->ontrack);

    // 200 OK
    send_json_response(200, [
        'success' => true,
        'message' => 'Ontrack updated',
        'payload' => $ontrack
    ]);
} catch (Exception $e) {
    // 500 Internal Server Error
    send_json_response(500, [
        'success' => false,
        'message' => $e->getMessage()
    ]);
}
