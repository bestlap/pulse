<?

$SESSION_LIMIT = 3; // Assuming 3 sessions total

$request_data = read_request_data('session.json');

try {
    $session = $request_data->{'payload'};

    $pitlaneDataFile = __CONFIG__['pitlaneDataFile'];
    $numbersDataFile = __CONFIG__['numbersDataFile'];
    $sessionDataFile = __CONFIG__['sessionDataFile'];

    $sessionData = read_data_file($sessionDataFile);

    if ($session->complete == false) {
        throw new Exception('Session is not complete');
    }

    if ($sessionData['heat'] == 0) {
        throw new Exception('Session is not started');
    }

    if ($sessionData['id'] >= $SESSION_LIMIT) {
        throw new Exception('All sessions are completed');
    }

    $next = [
        'id' => $sessionData['id'] + ($sessionData['final'] ? 0 : 1),
        'heat' => 0,
        'complete' => $sessionData['final'] ? true : false,
        'final' => $sessionData['id'] >= $SESSION_LIMIT,
    ];

    $numbersData = read_data_file($numbersDataFile);
    $pitlaneData = read_data_file($pitlaneDataFile);

    $pitlane = [
        'numbers' => $numbersData,
        'session' => $next,
        'ontrack' => $pitlaneData['ontrack'],
    ];

    save_data_file($sessionDataFile, $next);
    save_data_file($pitlaneDataFile, $pitlane);

    send_json_request(getenv('URL_ECHO') . '/session', $next);

    // 200 OK
    send_json_response(200, [
        'success' => true,
        'message' => 'Session completed',
        'payload' => $next
    ]);
} catch (Exception $e) {
    // 500 Internal Server Error
    send_json_response(500, [
        'success' => false,
        'message' => $e->getMessage()
    ]);
}
