<?php

require_once __DIR__ . '/config.php';

function get_route_handler($server_config, $request_method, $request_path) {
    // Supported endpoints and request methods
    $route_handlers = $server_config['routes'];

    // Default route handler
    $route_handler = $server_config['defaultRoute'];

    // Find specific route handler
    $route_options = $route_handlers[$request_path] ?? null;

    if ($route_options != null) {
        // Collect all supported request methods for this route
        $allow_methods = implode(', ', array_keys($route_options));

        // CORS preflight request?
        if ($request_method == 'OPTIONS') {
            http_response_code(204); // No Content

            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header('Access-Control-Allow-Origin: ' . getenv('URL_MARSHAL'));
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: ' . $allow_methods);
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header('Access-Control-Allow-Headers: Content-Type');
            }

            exit;

        } elseif (array_key_exists($request_method, $route_options)) {
            $route_handler = $route_options[$request_method];

            if (is_array($route_handler)) {
                $default_value = $route_handler[1] ?? '';
                $route_handler = $route_handler[0];
            }

            $ext = pathinfo($route_handler, PATHINFO_EXTENSION);

            switch ($ext) {
                case 'php':
                    break;

                case 'json':
                    $data = read_data_file($route_handler, false) ?? $default_value;

                    send_json_response(200, $data);

                    exit;

                default:
                    http_response_code(500); // Internal Server Error

                    exit;
            }
        } else {
            http_response_code(405); // Method Not Allowed

            header('Allow: OPTIONS, ' . $allow_methods);

            exit;
        }
    }

    return $route_handler;
}

function route_request($config, $method, $uri) {
    $path = rtrim(parse_url($uri, PHP_URL_PATH), '/');
    $file = get_route_handler($config, $method, $path);

    require_once __ROUTES_DIR__ . $file;
}

route_request(__CONFIG__, $_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
