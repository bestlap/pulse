<?

function send_json_request($url, $json) {
    $data = is_string($json) ? $json : json_encode($json);

    $options = [
        'http' => [
            'method' => 'POST',
            'header' => [
                'Content-type: application/json',
                'Content-Length: ' . strlen($data)
            ],
            'content' => $data,
        ],
    ];

    $context = stream_context_create($options);

    return file_get_contents($url, false, $context);
}

function send_json_response($code, $data) {
    http_response_code($code);

    header('Content-Type: application/json');

    if (is_string($data)) {
        echo $data;
    } else {
        echo json_encode($data);
    }
}

function read_request_data($schema) {
    $data = json_decode(file_get_contents('php://input'));

    if (isset($schema)) {
        try {
            require_once __LIB_DIR__ . '/vendor/json-schema/validator.php';
            $validator = new Json\Validator(__SCHEMAS_DIR__ . $schema);
            $validator->validate($data);
        } catch (Exception $e) {
            // 400 Bad Request
            send_json_response(400, [
                'success' => false,
                'message' => $e->getMessage(),
                'request' => $data
            ]);

            exit;
        }
    }

    return $data;
}

function read_file($path) {
    if (file_exists($path)) {
        return file_get_contents($path);
    }

    return null;
}

function read_json_file($path, $assoc = false) {
    $data = read_file($path);

    if (isset($data)) {
        return json_decode($data, $assoc);
    }

    return null;
}

function save_json_file($file, $data) {
    file_put_contents($file, json_encode($data, JSON_PRETTY_PRINT));
}

function read_data_file($name, $decode = true) {
    $path = __FILES_DIR__ . $name;

    if ($decode) {
        return read_json_file($path, true);
    }

    return read_file($path);
}

function save_data_file($name, $data) {
    $path = __FILES_DIR__ . $name;

    save_json_file($path, $data);
}
