<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);

# Root dir for local data storage.
define('__DATA_DIR__', __DIR__ . '/data');

# Helpers, utils, and vendor library code.
define('__LIB_DIR__', __DIR__ . '/lib');

# Route handlers location. Note trailing slash!
define('__ROUTES_DIR__', __DIR__ . '/routes/');

# Race data files location. Note trailing slash!
define('__FILES_DIR__', __DATA_DIR__ . '/files/');

# Request schemas location. Note trailing slash!
define('__SCHEMAS_DIR__', __DATA_DIR__ . '/schema/');

require_once __LIB_DIR__ . '/vendor/dotenv.php';
require_once __LIB_DIR__ . '/utils.php';

use DevCoder\DotEnv;

(new DotEnv(__DIR__ . '/.env'))->load();

define('__CONFIG__', read_json_file(__DIR__ . '/config.json', true));
