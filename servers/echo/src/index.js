import express from 'express';
import http from 'http';
import { EventEmitter } from 'events';
import { Server } from 'socket.io';
import { jsonEmitter } from './middleware/emitter.js';

const PORT = process.env.PORT;

const app = express();
const server = http.createServer(app);
const radio = new EventEmitter();
const io = new Server(server, {
  cookie: false,
  cors: {
    origin: '*',
  },
});

// Unlimited connections
radio.setMaxListeners(Infinity);

// Decode JSON request data
app.use(express.json());

app.get('/', (req, res) => res.json({ status: 'ready' }));

// Handle POST requests from Chrono
app.post('/operator', jsonEmitter(radio, 'operator'));
app.post('/pitlane', jsonEmitter(radio, 'pitlane'));
app.post('/numbers', jsonEmitter(radio, 'numbers'));
app.post('/session', jsonEmitter(radio, 'session'));
app.post('/ontrack', jsonEmitter(radio, 'ontrack'));

// Set up Socket
io.on('connection', function (socket) {
  // Got internal event?
  radio.on('operator', function (data) {
    // Forward to Socket listeners
    io.to(socket.id).emit('operator', data);
  });

  radio.on('pitlane', function (data) {
    io.to(socket.id).emit('pitlane', data);
  });

  radio.on('numbers', function (data) {
    io.to(socket.id).emit('numbers', data);
  });

  radio.on('session', function (data) {
    io.to(socket.id).emit('session', data);
  });

  radio.on('ontrack', function (data) {
    io.to(socket.id).emit('ontrack', data);
  });
});

// Spin up the server
server.listen(PORT, function () {
  console.log(' < < >< <> ||', PORT);
});
