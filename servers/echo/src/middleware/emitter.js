export function jsonEmitter(eventEmitter, eventName) {
  return (request, response) => {
    // Check content type
    if (request.is('json')) {
      // Read the JSON data
      const { body: json } = request;

      // Respond back (echo)
      response.setHeader('Content-Type', 'application/json');
      response.status(201).send(json);

      // Emit internal event
      eventEmitter.emit(eventName, json);
    } else {
      // Bad Request
      response.status(400);
    }
  };
}
